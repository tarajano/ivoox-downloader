# Ivoox Downloader

The application ivoox-downloader is an easy way to automatically download the episodes of your favourite podcasts using Go.

## Configuration

The configuration folder contains a configuration file "config.json" that can be modified to meet the user requirements.
The parameters in the configuration file are:

* **BaseURL**

    This is the base URL of Ivoox platform. It should remain unchanged.

* **PodcastListFile**

    This is the file that contains the URLs of the podcasts you want to automatically download.
    It needs to be created and populated before executing the application.
    It is important to note that there should be one line per podcast URL.

* **DownloadHistoryFile**

    This is the file that contains the URLs of downloaded episodes, so the application won't download them again.
    If the file doesn't exist when the application is executed, then the application will create it automatically.
    Just make sure that the user executing the application has rights to write in the selected path.

* **DownloadFolder**

    This is the folder in which the episodes will be saved, using the episode title without spaces, symbols or special characters.
    If the folder doesn't exist when the application is executed, then the application will create it automatically.
    Just make sure that the user executing the application has rights to write in the selected path.

* **MaxParallelDownloads**

    This parameter set the maximum number of episodes being downloaded at the same time.
    It is not recommended to go over the number of processors in your system.

* **IterateOverPages**

    This parameter set whether the application analyses the page specified by the user only or every page of the podcasts.
    It is useful if you want to download all the episodes of a specific podcast/s.

## Scheduler

In linux systems, it is recommended to use cron to automatically execute the application periodically, so you always have the latest episodes.

## Resources 

* "github.com/PuerkitoBio/goquery" goquery - a little like that j-thing, only in Go
* "github.com/tkanos/gonfig" gonfig

## TO-DO

* Logs