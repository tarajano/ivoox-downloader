package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"regexp"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
	"github.com/tkanos/gonfig"
)

// Configuration holds the parameters needed to run the application
type Configuration struct {
	BaseURL              string
	PodcastListFile      string
	DownloadHistoryFile  string
	DownloadFolder       string
	MaxParallelDownloads int
	IterateOverPages     bool
}

// EpisodeInfo holds relevant information related to the episodes
type EpisodeInfo struct {
	EpisodeTitle   string
	EpisodeBaseURL string
	EpisodeFileURL string
}

// Declaration of a global variable to enable access to configuration parameters from anywhere in the code
var configuration = Configuration{}

func init() {
	readConfiguration()
}

func main() {

	// Declare the channels to enable concurrency
	podcastCh := make(chan string, 8)
	episodeCh := make(chan string, 8)
	dowloadCh := make(chan EpisodeInfo)

	// Declare a WaitGroup variable to wait for the goroutines to finish
	var wg sync.WaitGroup
	wg.Add(configuration.MaxParallelDownloads + 3)

	// Create the goroutines to start processing the items
	go readPodcastList(podcastCh, &wg)
	go processPodcasts(podcastCh, episodeCh, &wg)
	go processEpisodes(episodeCh, dowloadCh, &wg)
	for i := 0; i < configuration.MaxParallelDownloads; i++ {
		go downloadEpisodes(dowloadCh, &wg)
	}

	wg.Wait()
}

// readConfiguration reads the application's json configuration file directly an maps the parameters using the gonfig library.
func readConfiguration() {
	err := gonfig.GetConf("configuration/config.json", &configuration)
	if err != nil {
		log.Fatal(err)
	}
}

// readPodcastList reads the file with the podcasts' url and send them to a channel for processing.
// If the configuration parameter "IterateOverPages" is true, then the method will iterate over the podcast pages using a regex, sending them to the channel.
func readPodcastList(podcastCh chan<- string, wg *sync.WaitGroup) {

	defer wg.Done()
	file, err := os.Open(configuration.PodcastListFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Regex to iterate over podcasts' pages in case IterateOverPages is enabled
	re := regexp.MustCompile(`(_1.html$)`)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if scanner.Text() != "" {
			// If IterateOverPages is true, iterate over podcast pages using the file url as base, otherwise process the file url only
			if configuration.IterateOverPages == true {
				for i := 1; ; i++ {
					page := re.ReplaceAllString(scanner.Text(), fmt.Sprintf("_%d.html", i))
					response, err := http.Get(page)
					if err != nil {
						break
					}
					defer response.Body.Close()

					document, err := goquery.NewDocumentFromReader(response.Body)
					if episodeElements := document.Find("p.title-wrapper a"); episodeElements.Size() == 0 {
						break
					}
					podcastCh <- page
				}
			} else {
				podcastCh <- scanner.Text()
			}
		}
	}
	close(podcastCh)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

// processPodcasts analyzes the podcasts' url to get the episodes to download that hasn't been downloaded yet.
func processPodcasts(podcastCh <-chan string, episodeCh chan<- string, wg *sync.WaitGroup) {

	defer wg.Done()
	for podcast := range podcastCh {
		response, err := http.Get(podcast)
		if err != nil {
			log.Fatal(err)
		}
		defer response.Body.Close()

		// Search the response, grab and process all the href to specific episodes that hasn't already been downloaded
		document, err := goquery.NewDocumentFromReader(response.Body)
		document.Find("p.title-wrapper a").Each(func(i int, p *goquery.Selection) {
			episodeURL, ok := p.Attr("href")
			if ok && !hasBeenDownloaded(episodeURL) {
				episodeCh <- episodeURL
			}
		})
	}
	close(episodeCh)
}

// hasBeenDownloaded checks if a specific URL is already in the history of dowloaded podcasts' episodes.
func hasBeenDownloaded(url string) (result bool) {
	if _, err := os.Stat(configuration.DownloadHistoryFile); os.IsNotExist(err) {
		os.Create(configuration.DownloadHistoryFile)
	}
	b, err2 := ioutil.ReadFile(configuration.DownloadHistoryFile)
	if err2 != nil {
		log.Fatal(err2)
	}
	return strings.Contains(string(b), url)
}

// processEpisodes analyzes the episodes' urls to retrieve the link to download the episode's audio file and send it to a channel for processing.
func processEpisodes(episodeCh <-chan string, dowloaderCh chan<- EpisodeInfo, wg *sync.WaitGroup) {

	defer wg.Done()
	for episode := range episodeCh {
		episodeTitle := extractEpisodeTitle(episode)
		episodeDownloadURL := extractEpisodeDownloadURL(episode)
		dowloaderCh <- EpisodeInfo{episodeTitle, episode, episodeDownloadURL}
	}
	close(dowloaderCh)
}

// extractEpisodeTitle gets the episode, remove whitespaces and symbols and returns it.
func extractEpisodeTitle(episode string) string {
	response, err := http.Get(episode)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()
	document, err := goquery.NewDocumentFromReader(response.Body)

	symbolDelRegex := regexp.MustCompile(`[^\w]`)
	episodeTitleElement := document.Find("div.title h1")
	return symbolDelRegex.ReplaceAllString(strings.ReplaceAll(strings.TrimSpace(episodeTitleElement.Text()), " ", "_"), "")
}

// extractEpisodeDownloadURL extracts the URL that contains the information about the episode's audio file from the HTTP response, gets the information
// from that URL, finds the element containing the href to the audio file and returns it.
func extractEpisodeDownloadURL(episode string) string {
	response, err := http.Get(episode)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Regular expression to match the partial url that contains the download link for the episode file
	dLinkRegex := regexp.MustCompile(`\$\(\'\.downloadlink\'\)\.load\(\'([a-zA-Z0-9_.?=]*html)\?tpl2\=ok\'\)\;`)
	match := dLinkRegex.FindSubmatch(body)
	if match == nil {
		return ""
	}

	// Get the URL containing the file http element
	u, err := url.Parse(configuration.BaseURL)
	u.Path = path.Join(u.Path, string(match[1]))
	response2, err2 := http.Get(u.String())
	if err2 != nil {
		log.Fatal(err)
	}

	document, err := goquery.NewDocumentFromReader(response2.Body)
	episodeDownloadURLElement := document.Find("a#dlink")
	episodeDownloadURL, found := episodeDownloadURLElement.Attr("href")
	if found {
		return episodeDownloadURL
	}
	return ""
}

// downloadEpisodes extracts the episode audio file from the HTTP response, saves it to a file in the specified download folder and writes
// the episode url to the download history so it is not processed again. If the download folder does not exist in the system, it will create it.
func downloadEpisodes(dowloaderCh <-chan EpisodeInfo, wg *sync.WaitGroup) {

	defer wg.Done()
	for download := range dowloaderCh {

		response, err := http.Get(download.EpisodeFileURL)
		if err != nil {
			log.Fatal(err)
		}
		defer response.Body.Close()

		if _, err := os.Stat(configuration.DownloadFolder); os.IsNotExist(err) {
			os.MkdirAll(configuration.DownloadFolder, os.ModePerm)
		}

		u, err := url.Parse(configuration.DownloadFolder)
		u.Path = path.Join(u.Path, download.EpisodeTitle)
		out, err := os.Create(u.String() + ".mp3")
		if err != nil {
			log.Fatal(err)
		}
		defer out.Close()
		_, err = io.Copy(out, response.Body)
		f, err := os.OpenFile(configuration.DownloadHistoryFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		if _, err := f.WriteString(download.EpisodeBaseURL + "\n"); err != nil {
			log.Fatal(err)
		}
	}
}
